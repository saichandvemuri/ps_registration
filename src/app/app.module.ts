import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PsRegistrationComponent } from './ps-registration/ps-registration.component';
import { PsAuthorizationComponent } from './ps-authorization/ps-authorization.component';
import { RegistrationComponent } from './registration/registration.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { BasicInfoComponent } from './create_new_ps/basic-info/basic-info.component';
import { PayorPlanDetailsComponent } from './create_new_ps/payor-plan-details/payor-plan-details.component';
import { GuarantorDetailsComponent } from './create_new_ps/guarantor-details/guarantor-details.component';
import { AdmissionDetailsComponent } from './create_new_ps/admission-details/admission-details.component';
import { HeaderComponent } from './create_new_ps/header/header.component';
import { ZipcodeService } from './services/zipcode.service';
import { HttpClientModule } from '@angular/common/http';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { DisableControlDirective } from './directives/disable-control.directive';
import { DatePipe } from '@angular/common';
import { AuthorizationComponent } from './create_new_ps/authorization/authorization.component';
import { PsListingComponent } from './ps-listing/ps-listing.component';
import { LoginComponent } from './login/login.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';

@NgModule({
  declarations: [
    AppComponent,
    PsRegistrationComponent,
    PsAuthorizationComponent,
    GuarantorDetailsComponent,
    AdmissionDetailsComponent,
    RegistrationComponent,
    BasicInfoComponent,
    PayorPlanDetailsComponent,
    HeaderComponent,
    DisableControlDirective,
    AuthorizationComponent,
    PsListingComponent,
    LoginComponent,

  ],
  entryComponents: [
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AutocompleteLibModule,
    MaterialModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    TooltipModule.forRoot(),
    AngularMultiSelectModule,

  ],
  providers: [DatePipe,ZipcodeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
