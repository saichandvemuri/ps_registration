import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ZipcodeService } from './services/zipcode.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'angular-material';
  public baseUrl = 'assets/url.json';
  public res;
  constructor(private router: Router, private http: HttpClient, private service: ZipcodeService){}
  ngOnInit(): void {
    this.http.get(this.baseUrl).subscribe((res)=>{
      this.res = res;
      let webService = this.res.webServiceUrl;
     //  console.log(webService,"ts file")
      localStorage.setItem('webServiceUrl',webService );
      this.service.getUrl();
    })
  }
}
