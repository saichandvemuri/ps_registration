import { NgModule } from '@angular/core';

import {MatButtonModule,
   MatButtonToggleModule,
   MatIconModule,
   MatProgressSpinnerModule,
   MatBadgeModule,
   MatToolbarModule,
   MatSidenavModule,
   MatMenuModule,
   MatListModule,
   MatDividerModule,
   MatGridListModule,
   MatExpansionModule,
   MatCardModule,
   MatTabsModule,
   MatStepperModule,
   MatInputModule,
   MatSelectModule,
   MatDatepickerModule,
   MatNativeDateModule,
   MatTooltipModule,
   MatSnackBarModule,
   MatDialogModule,
   MatTableModule,
   MatFormFieldModule,
   MatAutocompleteModule,
   MatCheckboxModule,
   MatRadioModule,
   MatChipsModule

    } from '@angular/material';

  // import { MatBadgeModule} from '@angular/material/badge'
//  import {MatChipsModule} from '@angular/material/chips';



const MaterialComponents=[
  MatButtonModule,
  MatChipsModule,
  MatButtonToggleModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatBadgeModule,
  MatToolbarModule,
  MatSidenavModule,
  MatMenuModule,
  MatListModule,
  MatDividerModule,
  MatGridListModule,
  MatExpansionModule  ,
  MatCardModule,
  MatTabsModule,
  MatStepperModule,
  MatInputModule,
  MatSelectModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatTooltipModule,
  MatSnackBarModule,
  MatDialogModule,
  MatTableModule,
  MatFormFieldModule,
  MatAutocompleteModule,
  MatCheckboxModule,
  MatRadioModule
]
@NgModule({
  declarations: [],
  imports: [
    MaterialComponents
  ],
  exports:[
    MaterialComponents
  ]
})
export class MaterialModule { }
