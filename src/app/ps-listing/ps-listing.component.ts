import { Component, OnInit } from '@angular/core';
import { ZipcodeService } from '../services/zipcode.service';
declare var $:any;

@Component({
selector: 'app-ps-listing',
templateUrl: './ps-listing.component.html',
styleUrls: ['./ps-listing.component.scss']
})
export class PsListingComponent implements OnInit {

constructor(public service:ZipcodeService) { }
listingPageData: any;

ngOnInit(): void {
this.listingData();
this.navtabs();
this.icontoggle();
}
navtabs(){
$(function () {
$('[data-toggle="tooltip"]').tooltip()
})
}
icontoggle(){
$('.icon-toggle').click(function () {
if ($(this).parent().siblings("span").hasClass('show')) {
$(this).parent().siblings("span").addClass('hide');
$(this).parent().siblings("span").removeClass('show');

} else if ($(this).parent().siblings("span").hasClass('hide')) {
$(this).parent().siblings("span").addClass('show');
$(this).parent().siblings("span").removeClass('hide');
}
});
}
listingData() {
this.service.getPSListForCEAT().subscribe(data => {
this.listingPageData = data;
console.log(this.listingPageData)
});
}

}