import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PsListingComponent } from './ps-listing.component';

describe('PsListingComponent', () => {
  let component: PsListingComponent;
  let fixture: ComponentFixture<PsListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PsListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PsListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
