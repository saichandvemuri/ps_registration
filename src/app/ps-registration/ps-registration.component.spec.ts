import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PsRegistrationComponent } from './ps-registration.component';

describe('PsRegistrationComponent', () => {
  let component: PsRegistrationComponent;
  let fixture: ComponentFixture<PsRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PsRegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PsRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
