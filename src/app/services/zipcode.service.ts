import { Injectable, ErrorHandler } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, Subject } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
 providedIn: 'root'
})
export class ZipcodeService {
 public updateBasicInfo=new Subject();
 public updateBasicInfo$=this.updateBasicInfo.asObservable();
 public responseData: any;
 public lookUp;
 public getGuarantor;
 getPSListFor;
 public lookUpDetails;
 getUrl() {
    this.lookUpDetails = localStorage.getItem('webServiceUrl');
  // console.log(this.lookUpDetails, "lookupdetails");
 }
 public jsonObj = { 'userId': '47' };
 public zip;

 constructor(private http: HttpClient) { }
 public getLookupDetails(): Observable<any> {
   this.getUrl()
  // return this.http.get('assets/data.json')
   return this.http.get(this.lookUpDetails + "/getLookupDetails?jsonObj=" + JSON.stringify(this.jsonObj) ).pipe(catchError(this.errorHandler));
 }
 public getZipcodeDetails(zipCode): Observable<any> {
   this.getUrl()
   this.zip = {'zipCode':zipCode}
   // const zipcodeDetails1 = `http://poc.aquilasoftware.com/pocextacc-webservices_9.2/telephony/getZipCodeDetails?jsonObj={%20%22zipCode%22:%22${zipCode}%22%20}`;
   // return this.http.get(zipcodeDetails1)
   return this.http.get(this.lookUpDetails + '/getZipCodeDetails?jsonObj=' + JSON.stringify(this.zip)).pipe(catchError(this.errorHandler));
 }

 public savePs(ob) {
  return this.http.get(this.lookUpDetails + '/savePsDetails?jsonObj=' + ob).pipe(catchError(this.errorHandler))
 // return this.http.get(this.lookUpDetails+'/savePsDetails?jsonObj=',ob)

 }
 public saveGuarantor(obj) {
   return this.http.get(this.lookUpDetails + '/saveGuarantorDetails?jsonObj=' + obj).pipe(catchError(this.errorHandler))
//   return this.http.get(this.lookUpDetails+'/saveGuarantorDetails?jsonObj=',obj)

  }
  private errorHandler(error: HttpErrorResponse) {
   console.log('error in API service', error);
   return throwError(error);
 }
 public getPsDetails(getPs){

   return this.http.get(this.lookUpDetails + '/getPsDetails?jsonObj=' + getPs).pipe(catchError(this.errorHandler));

 }
 // public getGuarantorDetails(getGuarantor){
 //   this.getUrl();
 //   this.getGuarantor = {'psId' : getGuarantor};

 //   return this.http.get( this.lookUpDetails + 'getGuarantorDetails?jsonObj=' + this.getGuarantor).pipe(catchError(this.errorHandler));

 // }
 public getGuarantorDetails(){
   this.getUrl();
   this.getGuarantor = {'psId' : 23159};

   return this.http.get( this.lookUpDetails + '/getGuarantorDetails?jsonObj=' + JSON.stringify(this.getGuarantor) ).pipe(catchError(this.errorHandler));
  // http://poc.aquilasoftware.com/pocextacc-webservices_9.2/telephony/getGuarantorDetails?jsonObj={%20%22psId%22:%2223159%22%20}
 }

  public getPSListForCEAT(){
   this.getUrl();
   this.getPSListFor = {'userId' : '129'};
   return this.http.get( this.lookUpDetails + '/getPSListForCEAT?jsonObj=' + JSON.stringify(this.getPSListFor) ).pipe(catchError(this.errorHandler));
 }

}