import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { ZipcodeService } from '../../services/zipcode.service';
import { DatePipe } from '@angular/common';
import { switchMap, debounceTime, tap, finalize } from 'rxjs/operators';
import { Observable } from 'rxjs'
import { Router } from '@angular/router';

@Component({
selector: 'app-basic-info',
templateUrl: './basic-info.component.html',
styleUrls: ['./basic-info.component.scss']
})
export class BasicInfoComponent implements OnInit {

title = 'Form';
basicForm: FormGroup;
salutationid: number;
lookupDetails: any;
saluationId: any;
addressTypeList: any;
maritalStatusList: any;
raceId: any;
phoneTypeList: any;
basicResponse;
genderId: any;
Keyword = 'label';
data;
addressid;
psIdBasic;
locationName;
addressIdBasic;
genderName;
siteId;
raceid;
genderid;
fill;
ob;
city;
zipDetails;
country;
timeZone;
userMappedOffices;
state;
county;
regis;
keyword1 = 'siteName';
user;
all: any = [];
phone;
submitted = false;
private basicPreviousDetails: any;
private previousPsDetails: any;
constructor(private fb: FormBuilder, public service: ZipcodeService, public date: DatePipe, private router:Router) { }
ngOnInit() {
this.service.updateBasicInfo$.subscribe(res=>{
console.log(res);
res?this.previousBasicInfo():null;


})
console.log("basic")
this.basicForm = this.fb.group({
saluationId: ['', Validators.required],
site: ['', Validators.required],
genderId: ['', Validators.required],
lastName: ['', Validators.required],
firstName: ['', Validators.required],
raceId: ['', Validators.required],
ssn: [''],
number: ['', Validators.required],
maritalStatusList: ['', Validators.required],
addressTypeList: ['', Validators.required],
phoneTypeList: ['', Validators.required],
city: ['', Validators.required],
zipcode: ['', Validators.required],
country: ['', Validators.required],
county: ['', Validators.required],
state: ['', Validators.required],
timeZone: ['', Validators.required],
lane: ['', Validators.required],
dob: ['', Validators.required],
siteName: ['', Validators.required]
});
this.basicDetails();
}
get f() {
return this.basicForm.controls;

}
dob1;
onSubmit() {
if (this.basicForm.valid) {
this.regis = this.basicForm.value;

this.submitted = true;
this.basicForm.value.timeZone = this.zipDetails.timeZoneId;
this.basicForm.value.county = this.zipDetails.countyId;
this.basicForm.value.siteName = this.zipDetails.siteName;
this.dob1 = this.date.transform(this.basicForm.value.dob, 'MM/dd/yyyy');
console.log(this.dob1)
const obj = this.basicForm.value;
obj.stateName = this.zipDetails.state;
obj.countyName = this.zipDetails.county;
obj.timeZone = this.zipDetails.timeZone;
obj.addressTypeList1 = this.locationName;
obj.numberName = this.phone;
obj.site = this.siteId;
obj.dob1 = this.dob1;
obj.genderName = this.genderName;
// debugger;


// console.log(this.basicForm.value)
localStorage.setItem('regis', JSON.stringify(obj));
this.user = JSON.parse(localStorage.getItem('regis'));
// tslint:disable-next-line: align
const jsonObj = {
"saluationId": (this.basicForm.value.saluationId),
"genderId": (this.basicForm.value.genderId),
"lastName": (this.basicForm.value.lastName),
"firstName": (this.basicForm.value.firstName),
"raceId": (this.basicForm.value.raceId),
"maritalStatusID": (this.basicForm.value.maritalStatusList),
"dob": this.date.transform(this.basicForm.value.dob, 'MM/dd/yyyy'),
// "ssn":369852193,
// "ssn": (this.basicForm.value.ssn),
"locationId": (this.basicForm.value.addressTypeList),
"city": (this.basicForm.value.city),
"addressLine": (this.basicForm.value.lane),
"zipcode": (this.basicForm.value.zipcode),
"phoneTypeid": (this.basicForm.value.phoneTypeList),
"phone": (this.basicForm.value.number),
"officeId": (this.siteId),
"updatedUserId": 47
};
console.log(JSON.stringify(jsonObj));
let parameters = JSON.stringify(jsonObj)
try {
this.service.savePs(parameters).subscribe(res => {
this.basicResponse = res;
console.log(res, 'getting the psId details')
if (Object.keys(this.basicResponse).length !== 0) {
sessionStorage.setItem('psDetails', JSON.stringify(this.basicResponse));
this.router.navigateByUrl('registration-re/child-guarantor')
}
});
localStorage.setItem('regis1', JSON.stringify(this.basicForm.value));
} catch (error) {
console.log(error)
}

} else {
alert('Fill the required fields');
this.router.navigateByUrl('registration-re/child-guarantor')
}

}

basicDetails() {
this.service.getLookupDetails().subscribe(data => {
this.lookupDetails = data;
console.log(this.lookupDetails);
this.saluationId = this.lookupDetails.salutationList;
this.userMappedOffices = this.lookupDetails.userMappedOffices;
this.addressTypeList = this.lookupDetails.addressTypeList;
this.maritalStatusList = this.lookupDetails.maritalStatusList;
this.raceId = this.lookupDetails.raceList;
this.phoneTypeList = this.lookupDetails.phoneTypeList;
this.genderId = this.lookupDetails.genderList;
});
}

selectChange(event, field) {

if (field === 'genderId') {
this.basicForm.get('genderId').setValue(event.id);
this.genderName = event.label;
}
if (field === 'site') {
this.basicForm.get('site').setValue(event.id),
this.siteId = event.id
// console.log(this.siteId);
}
if (field === 'raceId') {
this.basicForm.get('raceId').setValue(event.id);
}
if (field === 'maritalStatusList') {
this.basicForm.get('maritalStatusList').setValue(event.id);
}
if (field === 'saluationId') {
this.basicForm.get('saluationId').setValue(event.id);
}
if (field === 'addressTypeList') {
this.basicForm.get('addressTypeList').setValue(event.id);
this.locationName = event.label;

}
if (field === 'phoneTypeList') {
this.basicForm.get('phoneTypeList').setValue(event.id);
this.phone = event.label;
}
if (field === 'state') {
this.basicForm.get('phoneTypeList').setValue(event.id);
}

}

getzip(event) {
const zip = this.basicForm.get('zipcode').value;
// console.log(zip)
if (zip.length === 5) {
this.service.getZipcodeDetails(zip).subscribe(data => {
this.zipDetails = data;
this.city = this.zipDetails.city;
this.country = this.zipDetails.country;
this.state = this.zipDetails.state;
this.timeZone = this.zipDetails.timeZone;
this.county = this.zipDetails.county;
});
}
}
private previousBasicInfo(){
this.previousPsDetails= JSON.parse(sessionStorage.getItem('psDetails'));
let parameters= { 'psId': this.previousPsDetails.psId }
try {
this.service.getPsDetails(JSON.stringify(parameters)).subscribe(res=>{
this.basicPreviousDetails = res;
// this.guarantorForm.get('addressTypeList').setValue(this.basicPreviousData.locationName);
// this.guarantorForm.get('number').setValue(this.basicPreviousData.PHONE);
// this.guarantorForm.get('zipcode').setValue(this.basicPreviousData.ZIPCODE);
// this.guarantorForm.get('county').setValue(this.basicPreviousData.county);
// this.guarantorForm.get('country').setValue(this.basicPreviousData.country);
// this.guarantorForm.get('timeZone').setValue(this.basicPreviousData.timezone);
// this.guarantorForm.get('state').setValue(this.basicPreviousData.state);
// this.guarantorForm.get('lane').setValue(this.basicPreviousData.street);
// this.guarantorForm.get('lastName').setValue(this.basicPreviousData.lastname);
// this.guarantorForm.get('firstName').setValue(this.basicPreviousData.firstname)
// this.guarantorForm.get('city').setValue(this.basicPreviousData.county)

// this.guarantorForm.get('phoneTypeList').setValue(this.basicPreviousData.PHONETYPE)
// this.guarantorForm.get('saluationId').setValue(this.basicPreviousData.SALUTATION)

})
} catch (error) {
console.log(error)
}
}
}