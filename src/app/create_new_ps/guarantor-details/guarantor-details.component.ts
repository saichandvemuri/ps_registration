import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { ZipcodeService } from '../../services/zipcode.service';
import { Router } from '@angular/router';

@Component({
selector: 'app-guarantor-details',
templateUrl: './guarantor-details.component.html',
styleUrls: ['./guarantor-details.component.scss']
})
export class GuarantorDetailsComponent implements OnInit {
@ViewChild('relationName', { static: true }) relationAuto
@ViewChild('ocuupationName', { static: true }) ocuupationAuto
guarantorForm: FormGroup;
city;
zipDetails1;
country;
gtrState;
psDetails;
timeZone;
state;
county;
submittedGuarantor = false;
occupationList;
relationId;
addressTypeList: any;
locationName;
public basicPreviousData: any;
phoneTypeList: any;
Keyword = 'label';
addressid;
relationName;
raceid;
relationshipList;
genderid;
fill;
checked = false;
lname;
regis;
guarantorCountry;
lookupDetails;
phone;
occupationName;
checked1 = false;
s;

user;
all: any = [];
submitted = false;
basicResponse: Object;
psIdBasic: any;
addressIdBasic: any;
I: any[];
previousPsDetails: any;
guarantorResponse: Object;
constructor(private fb: FormBuilder, public service: ZipcodeService, private router:Router ) { }
ngOnInit() {
console.log("guarantor")
this.user = JSON.parse(localStorage.getItem('regis'));
this.guarantorForm = this.fb.group({
saluationId: ['', Validators.required],
relationshipList: ['', Validators.required],
lastName: ['', Validators.required],
firstName: ['', Validators.required],
ssn: [''],
number: ['', Validators.required],
occupationList: ['', Validators.required],
addressTypeList: ['', Validators.required],
phoneTypeList: ['', Validators.required],
city: ['', Validators.required],
zipcode: ['', Validators.required],
country: ['', Validators.required],
county: ['', Validators.required],
state: ['', Validators.required],
timeZone: ['', Validators.required],
lane: ['', Validators.required],
});
this.basicDetails();


}

get f() {

return this.guarantorForm.controls;

}

onSubmit() {
this.submitted = true;
console.log(this.guarantorForm.value)
if(this.guarantorForm.valid){
const obj = this.guarantorForm.value;
obj.addressTypeList1 = this.user.addressTypeList;
obj.numberId = this.user.phoneTypeList;
obj.relationshipList = this.relationId;
obj.relationname = this.relationName
obj.city = this.user.city;
obj.saluationId = this.user.saluationId
obj.psId = this.psIdBasic
this.previousPsDetails= JSON.parse(sessionStorage.getItem('psDetails'));

const jsonObj = {
"saluationId": (this.guarantorForm.value.saluationId),
"lastName": this.guarantorForm.value.lastName,
"firstName": this.guarantorForm.value.firstName,
"iRelationShipId": (this.guarantorForm.value.relationshipList),

"locationId": (this.guarantorForm.value.addressTypeList1),
"city": this.guarantorForm.value.city,
"addressLine": this.guarantorForm.value.lane,
"zipcode": this.guarantorForm.value.zipcode,
"phoneTypeid": (this.guarantorForm.value.numberId),
"phone": this.guarantorForm.value.number,
"updatedUserId": "47",
"psId": this.previousPsDetails.psId,
}
let param = JSON.stringify(jsonObj);
try {
this.service.saveGuarantor(param).subscribe(res => {
this.guarantorResponse = res;
console.log( this.guarantorResponse)
sessionStorage.setItem('guarantorDetails', JSON.stringify(this.guarantorResponse));
this.router.navigateByUrl('registration-re/child-admission')
});
} catch (error) {
console.log(error)
}

}else{
alert("Fill all the required fields")
}

}

basicDetails() {
this.service.getLookupDetails().subscribe(data => {
this.lookupDetails = data;
this.addressTypeList = this.lookupDetails.addressTypeList;
this.phoneTypeList = this.lookupDetails.phoneTypeList;
this.relationshipList = this.lookupDetails.relationshipList;
this.occupationList = this.lookupDetails.occupationList;

});
}
selectChange(event, field) {
if (field === 'addressTypeList') {
this.guarantorForm.get('addressTypeList').setValue(event.id);
this.locationName = event.label;
}
if (field === 'phoneTypeList') {
this.guarantorForm.get('phoneTypeList').setValue(event.id);
}
if (field === 'relationshipList') {
this.guarantorForm.get('relationshipList').setValue(event.id),
this.relationName = event.label,
this.relationId = event.id;
}
if (field === 'occupationList') {
// this.guarantorForm.get('occupationList').setValue(event.id);
this.occupationName = event.label;
}
if (field === 'phoneTypeList') {
this.guarantorForm.get('phoneTypeList').setValue(event.id);
this.phone = event.label;
}
}

getzip(event) {
const zip = this.guarantorForm.get('zipcode').value;
// console.log(zip)
if (zip.length === 5) {
this.service.getZipcodeDetails(zip).subscribe(data => {
this.zipDetails1 = data;
this.city = this.zipDetails1.city;
this.country = this.zipDetails1.country;
this.state = this.zipDetails1.state;
this.timeZone = this.zipDetails1.timeZone;
this.county = this.zipDetails1.county;
this.guarantorForm.get('state').setValue(this.city.label)

});
}
}

get1(event) {

if (event.target.checked) {
this.checked = true;
this.checked1 = true;
this.previousPsDetails= JSON.parse(sessionStorage.getItem('psDetails'));
let parameters= { 'psId': this.previousPsDetails.psId }
try {
this.service.getPsDetails(JSON.stringify(parameters)).subscribe(res=>{
this.basicPreviousData = res;
this.guarantorForm.get('addressTypeList').setValue(this.basicPreviousData.locationName);
this.guarantorForm.get('number').setValue(this.basicPreviousData.PHONE);
this.guarantorForm.get('zipcode').setValue(this.basicPreviousData.ZIPCODE);
this.guarantorForm.get('county').setValue(this.basicPreviousData.county);
this.guarantorForm.get('country').setValue(this.basicPreviousData.country);
this.guarantorForm.get('timeZone').setValue(this.basicPreviousData.timezone);
this.guarantorForm.get('state').setValue(this.basicPreviousData.state);
this.guarantorForm.get('lane').setValue(this.basicPreviousData.street);
this.guarantorForm.get('lastName').setValue(this.basicPreviousData.lastname);
this.guarantorForm.get('firstName').setValue(this.basicPreviousData.firstname)
this.guarantorForm.get('city').setValue(this.basicPreviousData.county)

this.guarantorForm.get('phoneTypeList').setValue(this.basicPreviousData.PHONETYPE)
this.guarantorForm.get('saluationId').setValue(this.basicPreviousData.SALUTATION)
console.log(res,"ps details")
console.log('after form',this.guarantorForm.value)

})
} catch (error) {
console.log(error)
}


} else {
this.checked = false;
this.checked1 = false;
this.guarantorForm.get('addressTypeList').setValue('');
this.guarantorForm.get('phoneTypeList').setValue('');
this.basicPreviousData = null;
this.relationAuto.clear();
this.relationAuto.close();
this.ocuupationAuto.clear();
this.ocuupationAuto.close();
//this.get2('', false)
}
}

get2(event,) {
// console.log(event.target.checked)
// const event2 = event = ! null || undefined ? manualEvent : event.target.checked;
// if (event2) {
if (event.target.checked ) {
this.checked1 = true;
this.guarantorForm.get('addressTypeList').setValue(this.basicPreviousData.locationName);
this.guarantorForm.get('number').setValue(this.basicPreviousData.PHONE);
this.guarantorForm.get('zipcode').setValue(this.basicPreviousData.ZIPCODE);
this.guarantorForm.get('county').setValue(this.basicPreviousData.county);
this.guarantorForm.get('country').setValue(this.basicPreviousData.country);
this.guarantorForm.get('timeZone').setValue(this.basicPreviousData.timezone);
this.guarantorForm.get('state').setValue(this.basicPreviousData.state);
this.guarantorForm.get('lane').setValue(this.basicPreviousData.street);
this.guarantorForm.get('city').setValue(this.basicPreviousData.county)
this.guarantorForm.get('phoneTypeList').setValue(this.basicPreviousData.PHONETYPE)
this.guarantorForm.get('saluationId').setValue(this.basicPreviousData.SALUTATION)
}
else {
this.checked1 = false;
this.guarantorForm.get('addressTypeList').setValue('');
this.guarantorForm.get('phoneTypeList').setValue('');
this.guarantorForm.get('state').setValue('');
}
}
public getPreviousBasic(){
this.service.updateBasicInfo.next(true)
this.router.navigateByUrl('registration-re/child-basic')

}
}