import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { RegistrationComponent } from './registration/registration.component';
import { BasicInfoComponent } from './create_new_ps/basic-info/basic-info.component';
import { PayorPlanDetailsComponent } from './create_new_ps/payor-plan-details/payor-plan-details.component';
import { GuarantorDetailsComponent } from './create_new_ps/guarantor-details/guarantor-details.component';
import { AdmissionDetailsComponent } from './create_new_ps/admission-details/admission-details.component';
import { AuthorizationComponent } from './create_new_ps/authorization/authorization.component';
import { PsListingComponent } from './ps-listing/ps-listing.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
{ path: '', redirectTo: '/login', pathMatch: 'full' } ,

{path:"registration-re",component :RegistrationComponent,
children:[
{path:"child-guarantor",component :GuarantorDetailsComponent},
{path:"child-admission",component :AdmissionDetailsComponent },
{path:"child-payorplan",component :PayorPlanDetailsComponent},
{path:"child-basic",component :BasicInfoComponent},
{path:"child-authorization",component :AuthorizationComponent},
]},
{path:"list",component:PsListingComponent},
{path:"login",component:LoginComponent}
];

@NgModule({
imports: [RouterModule.forRoot(routes,{useHash:true})],
exports: [RouterModule]
})
export class AppRoutingModule { }